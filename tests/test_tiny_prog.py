import unittest

from src.tiny_prog.tiny_prog import increment


class TestTinyProg(unittest.TestCase):

    def test_increment(self):
        self.assertEqual(increment(3),4)


if __name__ == '__main__':
    unittest.main()